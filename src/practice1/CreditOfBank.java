package practice1;

/**
 * Created by Vladimir on 11.09.2016.
 */
public class CreditOfBank {

    public static double bodyOfCreditInMonth (double body, int periodOfCredit, int month){
        double payment = body/periodOfCredit;
        for (int i = 1; i < month; i++) {
               body -= payment;
        }
        return body;
    }

    public static double percentOfCreditInMonth (double body, double percentRate, int periodOfCredit, int month){
        double payment = body/periodOfCredit;
        double percent=0;
        for (int i = 1; i < month; i++) {
            body -= payment;
        }
            percent = body*(percentRate/12/100);

        return percent;
    }


    public static double allPercentOfCredit (double body, double percentRate, int periodOfCredit, int month){
        double payment = body/periodOfCredit;
        double allPercent=0;
        for (int i = 1; i <= periodOfCredit; i++) {
            allPercent += body*(percentRate/12/100);
            body -= payment;
        }


        return allPercent;
    }

}
