package practice1;

/**
 * Created by Vladimir on 11.09.2016.
 */
public class CreditOfBankTest {
    public static void main(String[] args) {
        double bodyOfCredit=12000;
        double percentRateOfCreditInYear=18;
        int periodOfCreditMonth=6;

        for (int i = 1; i <= periodOfCreditMonth; i++) {
            System.out.print("In month " + i + ": " + "bodyOfCreditInMonth= " + CreditOfBank.bodyOfCreditInMonth(bodyOfCredit, periodOfCreditMonth, i) + " ");
            System.out.println("percentOfCreditInMonth= "  + CreditOfBank.percentOfCreditInMonth(bodyOfCredit, percentRateOfCreditInYear, periodOfCreditMonth, i));


        }

        /*System.out.println("bodyOfCreditInMonth= " + CreditOfBank.bodyOfCreditInMonth(12000, 6, 1));
        System.out.println("percentOfCreditInMonth= " + CreditOfBank.percentOfCreditInMonth(12000, 18, 6, 1));*/

        System.out.println("allPercentOfCredit= " + CreditOfBank.allPercentOfCredit(18000, 18, 6, 6));


    }
}
