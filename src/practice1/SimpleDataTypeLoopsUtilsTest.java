package practice1;

/**
 * Created by Vladimir on 10.09.2016.
 */
public class SimpleDataTypeLoopsUtilsTest {
    public static void main(String[] args) {


        System.out.println("~~~~ Test for 1-th task~~~");
        SimpleDataTypeLoopsUtils.printData();

        System.out.println("~~~~ Test for 2-th task~~~");
        SimpleDataTypeLoopsUtils.printFloat();

        System.out.println("~~~~ Test for 3-th task~~~");


        SimpleDataTypeLoopsUtils.printShortData1();
        SimpleDataTypeLoopsUtils.printShortData2();
        SimpleDataTypeLoopsUtils.printShortData3( 2.45f, 7);
        SimpleDataTypeLoopsUtils.printShortData4((byte)23, (short)342 );
        SimpleDataTypeLoopsUtils.printShortData5(2f, 444343.);
        System.out.println();

        System.out.println("~~~~ Test for 4-th task~~~");

        System.out.println("Our triangle is right - " + SimpleDataTypeLoopsUtils.trangle(3, 4, 6));


        System.out.println("~~~~ Test for 5-th task~~~");
        int start = 0;
        int end = 20;
        int result = SimpleDataTypeLoopsUtils.intNumbersSum(start, end);
        System.out.println("Sum int numbers from " + start + " to " + end + " is "+ + result);

        System.out.println("~~~~ Test for 6-th task~~~");
        int start2 = 1;
        int end2 = 4;
        int result2 = SimpleDataTypeLoopsUtils.evenNumbersSum(start2, end2);
        System.out.println("Sum even numbers from " + start2 + " to " + end2 + " is "+ + result2);

        System.out.println("~~~~ Test for 7-th task~~~");
        int start3 = -4;
        int end3 = 20;
        int result3 = SimpleDataTypeLoopsUtils.simpleNumbersSum(start3, end3);
        System.out.println("Sum symple numbers from " + start3 + " to " + end3 + " is "+ + result3);

        System.out.println("~~~~ Test for 8-th task~~~");
        int a = 3;
        int b = 3;
        int c = 5;
        boolean result4 = SimpleDataTypeLoopsUtils.sumTwoVariable(a, b, c);
        System.out.println("Sum of two variables is equal third variable is " + result4);

        System.out.println("~~~~ Test for 9-th task~~~");
        System.out.println("AverageValueNumbers is " + SimpleDataTypeLoopsUtils.averageValueNumbers(6,3));


    }
}
