import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by Vladimir on 07.09.2016.
 */
public class HomeWork1 {

    static double  task1Square (double a){
        double S=a*a;
        return S;
    }
    static double task2CarsDistance (double V1, double V2,double S, double T){

        double d= Math.abs(S-T*(V1+V2));
        return d;
    }
    static double task3SquareEquation1(int A,int B,int C){
        double x1=((-B + Math.sqrt(B*B-4*A*C))/(2*A));
        return x1;

    }
    static double task3SquareEquation2(int A,int B,int C){

        double x2=((-B - Math.sqrt(B*B-4*A*C))/(2*A));
        return x2;

    }
    static  int task4 (int a){
        if (a>0) a+=1;
        else if (a<0) a-=2;
                else a=10;
        return a;
    }
    static double task5 (double A, double B, double C){
        double sum = 0;
        if (A<B & A<C) sum=B+C;
        if (B<A & B<C) sum=A+C;
        if (C<A & C<B) sum=A+B;
        if (A==B & B==C) sum=A+A;
        if (A==B & B<C) sum=A+C;
        if (B==C & C<A) sum=B+A;
        if (A==C & C<B) sum=A+B;
        return sum;
    }
    static double task52 (double A, double B, double C) {
        double sum = 0;
        if (A <= B) {
            if (A <= C) sum = B + C;
            else sum = B + A;
        }
        if (B <= A) {
            if (B <= C) sum = A + C;
            else sum = A + B;
        }
        if (C <= A) {
            if (C <= B) sum = A + B;
            else sum = A + C;
        }

        return sum;
    }

    static String task6 (int a){
            String note=null;
        if (a == 0) note="нулевое число";
        if (a>0) {
            if (a % 2 == 0) note = "положительное четное число";
            else note = "положительное нечетное число";
        }
        if (a<0) {
            if (a % 2 == 0) note = "отрицательное четное число";
            else note = "отрицательное нечетное число";
        }


        return note;
    }
    static boolean task7 (int A, int B){
        return ((A>2) & (B<=3));
    }

    static boolean task8 (int A, int B, int C){
        return ((A<B) & (B<C));
    }

    static int task9 (int A, int B, int C, int D){
        int n=0;
        if ((A==B)&(B==C))n=4;
        if ((A==C)&(C==D))n=2;
        if ((A==B)&(B==D))n=3;
        if ((B==C)&(C==D))n=1;
        return n;

    }
    static String task10 (int K){
        String mark=null;
        if (K==1) mark="плохо";
        else if(K==2) mark="неудовлетворительно";
        else if (K==3) mark="удовлетворительно";
        else if (K==4) mark="хорошо";
        else if (K==5) mark="отлично";
        else mark="ошибка";

        return mark;
    }

    static String task11 (int n){
        String season=null;
        if (n==12|| n==1|| n==2) season="Winter";
        else if (n==3|| n==4|| n==5) season="Spring";
        else if (n==6|| n==7|| n==8) season="Summer";
        else if (n==9|| n==10|| n==11) season="Autumn";
        else season="incorrect season";


        return season;
    }
    static double task12 (int N, double A, double B){
        double result=0;
        if (N==1) result=A+B;
        else if (N==2) result=A-B;
        else if (N==3) result=A*B;
        else if (N==4) result=A/B;

        return result;
    }

    static int [] numbersTask13(int A, int B){
        int[] numbers=new int[B-A+1];
        for (int i = 0; i < B-A+1; i++) {
            numbers[i]=i+A;
        }
        return numbers;
    }
    static int numbTask13(int A, int B){
        return (B-A+1);
    }
    static int task14 (int A, int B){
        int sum=0;
        for (int i = A; (i>= A) & (i <= B); i++) {
            sum+=i;

        }

        return sum;
    }
    static int task15 (int N){
        int mult=1;
        for (int i = 2; i<=N; i++) {
            mult*=i;

        }

        return mult;
    }
    static double task16 (double A, double B){
        while (A>B){
            A-=B;
        }
        return A;
    }
    static int task17 (int N){
        int K=1;
        while (3*K<=N){
        K+=1;
        }
     return K;
    }
   /* static int[] task18 (int N){
        int i=0;
        int numbers[] = new int[i];
        while (N/10!=0){
            numbers=new int[i];
            numbers[i]=N%10;
            N/=10;
            i+=1;

        }
        return  numbers;


    }*/
   /* throws java.io.IOException{
    System.out.println("Enter a");
        double a;
        a= System.in.read();
        //System.out.println(square (a));
        System.out.println(a);

            }*/
   public static void main(String[] args) {
       System.out.println("Task1 square=" + task1Square(13.6));
       System.out.println("Task2 carsDistance=" + task2CarsDistance(60, 50, 300, 0.5));
       System.out.println("Task3 x1=" + task3SquareEquation1(56, 12345, 1));
       System.out.println("Task3 x2=" + task3SquareEquation2(56, 12345, 1));
       System.out.println("Task4 a=" + task4(0));
       System.out.println("Task5 with logic Sum of biggest number=" + task5(1, -9, 1));
       System.out.println("Task5 sum of biggest number=" + task52(1, -9, 1));
       System.out.println("Task 6 a-" + task6(0));
       System.out.println("Task 7 " + task7(3, 6));
       System.out.println("Task 8 " + task8(3, 6, 7));
       System.out.println("Task 9 порядковый номер " + task9(3, 7, 7, 7));
       System.out.println("Task 10 mark:" + task10(5));
       System.out.println("Task 11 season:" + task11(2));
       System.out.println("Task 12 result=" + task12(2, 2, 6));
       System.out.println("Task 13 numbers=" + Arrays.toString(numbersTask13(2, 10)) + " N=" + numbTask13(2, 10));
       System.out.println("Task 13 numbers=" + Arrays.toString(numbersTask13(2, 10)) + " N=" + Array.getLength(numbersTask13(2, 10)));
       System.out.println("Task 14 sum=" + task14(7, 10));
       System.out.println("Task 15 mult=" + task15(9));
       System.out.println("Task 16 segment=" + task16(11, 3));
       System.out.println("Task 17 K=" + task17(12));
        /*System.out.println("Task 18 numbers="+*//*Arrays.toString*//*(task18(12)));*/
       /*System.out.println("test=" + 17 % 10);*/

       System.out.print("Task 18: " );
       int N = 456789;
       if (N / 10 == 0) System.out.print(N);
       else {
           while (N / 10 != 0) {
               System.out.print(N % 10 + " ");

               N /= 10;
               if (N / 10 == 0) System.out.print(N);
           }


       }
       System.out.println();
       System.out.print("Task 19: " );
       int A = -2;
       int B = 5;
       int n = A;

       for (int i = A; i < B  ; i++) {
           System.out.print(n++ + " ");
           for (int j = 1; j < n-A+1; j++) {
               System.out.print(n + " ");
           }



       }
       System.out.print(n);
       System.out.println();
       }

   }





